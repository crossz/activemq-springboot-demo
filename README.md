# activemq-springboot-demo

## springboot 的方式

采用 spring 注释的方式来完成 mq 的配置.

- jmsListenerContainer 来对 listener 声明, 其间用 bean.setPubSubDomain(true) 来声明 topic 的模式;
- jmsMessagingTemplate 用来做发布消息的工具方法;
- containerFactory 和 destination 来对 consumer 进行配置, 使得 consumer 的方法变得异常简单;

 基本上, 只要是 config 设置好(mq 的和 consumer 的), producer 和 consumer 的实现就变得很清爽.

 ----

 ## activemq 的本地安装

> docker pull rmohr/activemq
> docker run -d -p 61616:61616 -p 8161:8161 rmohr/activemq

详见: https://hub.docker.com/r/rmohr/activemq
